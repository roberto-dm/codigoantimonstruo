## Proyecto antimonstruo
Esta es una aplicación con fines de ejemplo de la ponencia **Que tu código no se convierta en mostruo**. 
La intención es probar 3 herramientas principales para tener dominio y confianza a la hora de programar:
1. ESLint
2. JSDoc
3. Mocha

En el código, se encontrarán "detalles" por arreglar.

### Instalar la aplicación
Descargue los archivos o clone el repositorio git y ejecute:
```
npm install
```
dentro de la carpeta raíz del proyecto.
### Dependencias
Si usted quiere probar crear su propia aplicación, puede crear una carpeta nueva y desde una consola ubicada en la carpeta, ejecutar los siguientes comandos para instalar las dependencias:
```
npm init
npm install -S express
npm install -D chai chai-http eslint eslint-config-google jsdoc mocha sinon
```
### Comandos
Iniciar la aplicación y verla en el puerto 3000 del navegador web:
```
npm start
```
Iniciar los test:
```
npm test
```
Generar documentación:
```
npm docs
```
#### Nota

Si no le funciona el comando de crear la documentación, abra una consola en la carpeta raíz del proyecto y ejecute:
```
node_modules/.bin/jsdoc app -r -R readme.md
```
Podrá ver los archivos generados en el nuevo directorio *out* . Recomiendo abrir en el navegador el archivo *index.html*