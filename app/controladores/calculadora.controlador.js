/**
 * @module CalculadoraControlador
 * @export CalculadoraControlador
 */

/**
 * @class CalculadoraControlador
 * @classdesc Contiene métodos de una calculadora básica
 */
class CalculadoraControlador {
    /**
     * Devuelve la suma de dos números
     * @param {number} a Primer número
     * @param {number} b Segundo número
     * @return {number|string} resultado o mensaje de error
     */
    sumar(a, b) {
        const valor = Number(a) + Number(b);
        if (!isNaN(valor)) {
            return valor;
        } else {
            return "Sólo puedo sumar números.";
        }
    };

    restar(a, b) {
        var valor = Number(a) - Number(b);
        if (!isNaN(valor)) {
            return valor;
        } else {
            return "Sólo puedo restar números.";
        }
    };

    /**
     * Multiplica dos números
     * @param {number} a Primer número
     * @param {number} b Segundo número
     * @return {number|string} resultado o mensaje de error
     */
    multiplicar(a, b) {
        let resultado = 0;
        a = Number(a);
        b = Number(b);
        for(var i=0;i<b;i++) {
            resultado += a;
        }
        if (!isNaN(resultado)) {
            return resultado;
        } else {
            return "Sólo puedo multiplicar números.";
        }
    };
}
module.exports = new CalculadoraControlador();
