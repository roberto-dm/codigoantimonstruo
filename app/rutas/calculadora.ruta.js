/**
 * @module rutaCalculadora
 * @exports {Router} exporta ruta de express para /calculadora
 */

let router = require('express').Router();
const calculadora = require('../controladores/calculadora.controlador');

router.get('/suma/:a/:b', function(req, res, next) {
    var valor = calculadora.sumar(req.params.a, req.params.b);
    res.status(200).send({resultado: valor});
})

router.get('/resta/:a/:b', function(req, res, next) {
    var valor = calculadora.restar(req.params.a, req.params.b);
    res.status(200).send({resultado: valor});
})

router.get('/multiplica/:a/:b', function(req, res, next) {
    var valor = calculadora.multiplicar(req.params.a, req.params.b);
    res.status(200).send({resultado: valor});
})

router.get('/divide/:a/:b', function(req, res, next) {
    var valor = calculadora.dividir(req.params.a, req.params.b);
    res.status(200).send({resultado: valor});
})

module.exports = router;
