/**
 * Exporta un modulo con las rutas asignadas a la aplicacion de express
 * proveniente del host o entry point
 * @module Modulo asignador de rutas.
 * @param {express} app Aplicacion de express
 */
const inicio = require('./inicio.ruta');
const calculadora = require('./calculadora.ruta');

module.exports.asignarRutas = function(app) {
    app.use('/', inicio);
    app.use('/calculadora', calculadora);
};
