const express = require('express');
const rutas = require('./rutas');

let app = express();

rutas.asignarRutas(app);

app.listen(3000);
