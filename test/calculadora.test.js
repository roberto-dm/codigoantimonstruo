const chai = require('chai');

const calculadora = require('../app/controladores/calculadora.controlador');

describe('Controlador Calculadora', function(listo) {
    it('Deberia sumar', () => {
        const resultado = calculadora.sumar(2,3);
        chai.expect(resultado).to.be.a('number');
        chai.expect(resultado).to.be.equal(5);
    });
    it('Deberia restar'/* , () => {
        const resultado = calculadora.restar(10, 3);
        chai.expect(resultado).to.be.a('number');
        chai.expect(resultado).to.be.equal(7);
    } */);
    it('Deberia multiplicar', () => {
        const resultado = calculadora.multiplicar(3, 3);
        chai.expect(resultado).to.be.a('number');
        chai.expect(resultado).to.be.equal(9);
    });
    it('Deberia dividir', () => {
        const resultado = calculadora.dividir(2, 3);
        chai.expect(resultado).to.be.a('number');
        chai.expect(resultado).to.be.equal(5);
    });
});
