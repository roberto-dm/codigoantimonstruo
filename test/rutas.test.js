const chai = require('chai');
const chaiHttp = require('chai-http');
const express = require('express');
const sinon = require('sinon');

const calculadora = require('../app/controladores/calculadora.controlador');

chai.use(chaiHttp);


const rutas = require('../app/rutas/');
const app = express();
rutas.asignarRutas(app);

describe('Probar conexión principal', function(listo) {
    it('Aplicacion responde con estado 200', (listo) => {
        chai.request(app)
        .get('/')
        .end((err, res) => {
            chai.expect(res).to.have.status(200);
            listo();
        })
    });
});

describe('Rutas GET a /calculadora/*', function(listo) {
    it('Responde con 200 la ruta suma/2/3', (listo) => {
        chai.request(app)
        .get('/calculadora/suma/2/3')
        .end((err, res) => {
            chai.expect(res).to.have.status(200);
            listo();
        });
    });
    let resta;
    try {
        resta = sinon.stub(calculadora, 'restar');
    } catch (e) {}
    it('Responde con 200 la ruta resta/8/3', (listo) => {
        resta.returns(4);
        chai.request(app)
        .get('/calculadora/resta/8/2')
        .end((err, res) => {
            chai.expect(res).to.have.status(200);
            listo();
        });
    });
});
